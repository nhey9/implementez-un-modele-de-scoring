# basical imports
import pandas as pd
import time
import os
import re

# Kaggle kernel import
from contextlib import contextmanager

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

# Metrics
from sklearn.metrics import roc_auc_score, roc_curve
from sklearn.model_selection import cross_val_score


# Pretreatment
from sklearn.model_selection import KFold, StratifiedKFold
from sklearn.model_selection import train_test_split
from sklearn.impute import KNNImputer

# Models imports
from sklearn.svm import SVC
from sklearn.ensemble import GradientBoostingClassifier
from lightgbm import LGBMClassifier

from bayes_opt import BayesianOptimization, UtilityFunction


@contextmanager
def timer(title):
    t0 = time.time()
    print(title, end="")
    yield
    print(" - done in {:.0f}s".format(time.time() - t0))

    
def SVC_model(C, X_train, y_train, X_test, y_test):    
    # C: SVC hyper parameter to optimize for.
    model = SVC(C = C)
    model.fit(X_train, y_train)
    y_score = model.decision_function(X_test)
    f = roc_auc_score(y_test, y_score)
    return f
    

def fillna_fun(df, target_name='TARGET', threshold=10):
    """
    fill the dataframe,
    don't  touch to the TARGET column,
    remove col with more than 1/threshold NaN values.
    """
    df_v2 = df.copy()
    target = df_v2[target_name]
    df_v2 = df_v2.drop(target_name, axis=1)
    
    print('Start with ', (df_v2.isna().sum() > 1 ).sum(), ' columns with NaN and ', df_v2.isna().sum().sum(), ' cells.' , sep="")
    for col in df_v2:
        if (df_v2[col].isna().sum() > (len(df_v2)/ threshold) ):
            df_v2 = df_v2.drop(col, axis=1)
    print('Now there is ', (df_v2.isna().sum() > 1 ).sum(), ' columns with NaN and ', df_v2.isna().sum().sum(), ' cells.', sep="")
    
    #Imputing with default parameters 
    imputer = KNNImputer()

    #Reshaping to meet the dimensional requirement
    imputer.fit(df_v2)
    
    return pd.DataFrame(imputer.transform(df_v2), columns=df_v2.columns)    
    

def run_model(df, stratified = False, num_folds=2, need_fillna=True):
    print(df.shape)
    if need_fillna:
        with timer("Fillna"):
            df_filled = fillna_fun(df, threshold=25)
        df_filled.to_pickle('dataframes/df_fillna.pkl')
    else:
        df_filled = pd.read_pickle('dataframes/df_fillna.pkl')
    
    # Divide in training/validation and test data
    train_df = df[df['TARGET'].notnull()]
    test_df = df[df['TARGET'].isnull()]
    
    X = train_df.drop('TARGET', axis=1)
    y = train_df['TARGET']
    
    
        
    ## nhey debug
    X_new_col = [re.sub('[^A-Za-z0-9_]+', '', col) for col in X.columns]
    X = X.rename(columns = {X.columns[i]: X_new_col[i] for i in range(len(X_new_col))})
    ##
    
    print("Start of training. Train shape: {}, test shape: {}".format(train_df.shape, test_df.shape))
    
    if stratified:
        folds = StratifiedKFold(n_splits= num_folds, shuffle=True, random_state=42)
    else:
        folds = KFold(n_splits= num_folds, shuffle=True, random_state=42)
        
    C = 1
    #for train_index, test_index in folds.split(X):
    #    X_train, X_valid = X.iloc[train_index], X.iloc[test_index]
    #    y_train, y_valid = y.iloc[train_index], y.iloc[test_index]
        
        #####################################################
        ## This give 0.77 ROC AUC Score
        #clf = LGBMClassifier().fit(X_train, y_train)
        #y_score = clf.predict_proba(X_valid)[:, 1]
        #f = roc_auc_score(y_valid, y_score)
        #clf = GradientBoostingClassifier(n_estimators=100, learning_rate=1.0,
        #                                 max_depth=1, random_state=42).fit(X_train, y_train)
        
        
        
    clf = LGBMClassifier()
    scores = cross_val_score(clf, X, y, cv=5, scoring='roc_auc')

    print("For C = ", C, " -> ROC AUC score : ", scores, sep="")
    
def main():
    # Read the dataframe
    df = pd.read_pickle('dataframes/df.pkl')
    run_model(df)
    
if __name__ == "__main__":
    main()